﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading;
using System.Threading.Tasks;

namespace LargeFilesTest
{
    class Program
    {
        // Folder that contains the large files to upload
        public static readonly string LargeFilesFolderPath = @"C:\Users\JorgeGomez\Desktop\multiple-upload";
        // Base Url
        private static string BaseUrl = "http://jg-r7ga3ip/fotoweb/api/uploads";
        //Cokkie
        private static string Cookie = "FotoWebVersion=bdf8e2bb-c60e-4c12-b441-2156f0171049; SessionToken=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vamctcjdnYTNpcC9mb3Rvd2ViLyIsInN1YiI6IjE1MDAyIiwiZXhwIjoxNjMwMDg2OTA3LCJuYmYiOjE2MzAwNTgxMDcsImlhdCI6MTYzMDA1ODEwNywianRpIjoiMmhOQjdOUkVCMDZnTTVUWmNud0dzaGVqYjhwM0VZTmNQOEh3cUllUWhCUzZ5aE0yMVdhSkhyMkRFVzQ3bHRheCIsImF1ZCI6InNlc3Npb25fdG9rZW4iLCJzY29wZSI6WyJmdWxsX2FjY2VzcyJdfQ.VEVB7FQs4iHiTrVObcmTq32Se9nDyakGjpvLIpkDdKc";
        // XMP file name
        public static string XMPFileName = "XMP-file.xmp";

        public class UploadFile
        {
            private readonly uint _index;
            private readonly string _filePath;
            private readonly string _fullFileName;
            private readonly string _fileName;
            private readonly string _chunksFolderPath;
            private UploadInfo _uploadInfo;
            private static readonly SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(1, 1);
            private Stopwatch _stopwatch = new Stopwatch();

            public UploadFile(uint index, string filePath)
            {
                _index = index;
                _filePath = filePath;
                _fullFileName = _filePath.Split('\\')[_filePath.Split('\\').Length - 1];
                _fileName = _fullFileName.Split('.')[0];
                _chunksFolderPath = Path.Combine(LargeFilesFolderPath, _fileName);
            }

            public async Task<UploadInfo> CreateUploadTask()
            {
                var uriBuilder = new UriBuilder(BaseUrl);
                byte[] file;
                try
                {
                    file = File.ReadAllBytes(_filePath);
                }
                catch (FileNotFoundException ex)
                {
                    await _semaphoreSlim.WaitAsync();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.AppendLine($" ERROR! {ex.Message}");
                    await Log(stringBuilder);
                    _semaphoreSlim.Release();

                    return null;
                }

                // UPLOAD EXAMPLES

                // Upload a file
                string upload = "{" +
                    "\"destination\": \"/fotoweb/archives/5000-my-archive/im-folder/upload-folder/\", " +
                    "\"folder\": null, " +
                    "\"filename\": \"" + _fullFileName + "\", " +
                    "\"hasXmp\": false, " +
                    "\"fileSize\": " + file.Length + ", " +
                    "\"checkoutId\": null, " +
                    "\"metadata\": null, " +
                    "\"ignoreMetadata\": true, " +
                    "\"comment\": null " +
                    "}";

                // Upload a file creating a folder in the destination
                //string upload = "{" +
                //    "\"destination\": \"/fotoweb/archives/5000-my-archive/im-folder/upload-folder/\", " +
                //    "\"folder\": \"new-folder/\", " +
                //    "\"filename\": \"" + _fullFileName + "\", " +
                //    "\"hasXmp\": false, " +
                //    "\"fileSize\": " + file.Length + ", " +
                //    "\"checkoutId\": null, " +
                //    "\"metadata\": null, " +
                //    "\"ignoreMetadata\": true, " +
                //    "\"comment\": null " +
                //    "}";

                // Upload a file with metadata
                //string upload = "{" +
                //    "\"destination\": \"/fotoweb/archives/5000-my-archive/im-folder/upload-folder/\", " +
                //    "\"folder\": null, " +
                //    "\"filename\": \"" + _fullFileName + "\", " +
                //    "\"hasXmp\": false, " +
                //    "\"fileSize\": " + file.Length + ", " +
                //    "\"checkoutId\": null, " +
                //    "\"metadata\": " +
                //        "{ " +
                //            "\"fields\": " +
                //                " [ " +
                //                    "{ \"id\": 5, \"value\": \"Roadrunner\"}, " +
                //                    "{ \"id\": 80, \"value\": \"Wyle E. Coyote\", \"action\": \"add\"}, " +
                //                    "{ \"id\": 25, \"action\": \"erase\"}, " +
                //                    "{ \"id\": 25, \"action\": \"add\", \"value\": [\"chicken\", \"food\"]} " +
                //                " ] " +
                //        "}, " +
                //    "\"ignoreMetadata\": false, " +
                //    "\"comment\": null " +
                //    "}";

                // Upload a file with XMP file
                //string upload = "{" +
                //    "\"destination\": \"/fotoweb/archives/5000-my-archive/im-folder/upload-folder/\", " +
                //    "\"folder\": null, " +
                //    "\"filename\": \"" + _fullFileName + "\", " +
                //    "\"hasXmp\": true, " + 
                //    "\"fileSize\": " + file.Length + ", " +
                //    "\"checkoutId\": null, " +
                //    "\"metadata\": null, " +
                //    "\"ignoreMetadata\": false, " +
                //    "\"comment\": null " + 
                //    "}";

                var content = new StringContent(upload, Encoding.UTF8, "application/json");

                var response = await Request(HttpMethod.Post, uriBuilder.Uri.ToString(), content);
                if (response == null)
                {
                    return null;
                }

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseContent = response.Content.ReadAsStringAsync();
                    var uploadInfo = Newtonsoft.Json.JsonConvert.DeserializeObject<UploadInfo>(responseContent.Result);

                    await _semaphoreSlim.WaitAsync();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.AppendLine($" New background-task created with ID: {uploadInfo.Id}");
                    stringBuilder.AppendLine($" | FileName: {_fullFileName}");
                    stringBuilder.AppendLine($" | FileSize: {file.Length} bytes");
                    stringBuilder.AppendLine($" | Number of chunks: {uploadInfo.NumChunks}");
                    stringBuilder.AppendLine($" | Chunk size: {uploadInfo.ChunkSize} bytes");
                    stringBuilder.AppendLine($" | Has XMP file: {uploadInfo.HasXmp}");
                    await Log(stringBuilder);
                    _semaphoreSlim.Release();

                    return uploadInfo;
                }
                else
                {
                    var responseContent = response.Content.ReadAsStringAsync();
                    var failedResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<FailedResponse>(responseContent.Result);

                    await _semaphoreSlim.WaitAsync();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.AppendLine($" Response: {(Int32)response.StatusCode} - {response.StatusCode}");
                    stringBuilder.AppendLine($" | Response: {failedResponse.Heading} {failedResponse.Value}");
                    await Log(stringBuilder);
                    _semaphoreSlim.Release();

                    return null;
                }
            }

            private ConsoleColor GetColor()
            {
                return (ConsoleColor)Enum.GetValues(typeof(ConsoleColor)).GetValue(_index);
            }

            private async Task Log(StringBuilder stringBuilder)
            {
                Console.BackgroundColor = GetColor();
                Console.Write($"[{_index}]");
                Console.ResetColor();
                Console.Write(stringBuilder);
            }

            private async Task LogStatus(string status)
            {
                Console.BackgroundColor = GetColor();
                Console.Write($"[{_index}]");
                Console.ResetColor();
                Console.Write(" Background-task status: ");
                if (status.Equals("done"))
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(status);
                    Console.ResetColor();
                }
                else if (status.Equals("failed"))
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(status);
                    Console.ResetColor();
                }
                else
                {
                    Console.WriteLine(status);
                }
            }

            private async Task<HttpResponseMessage> Request(HttpMethod httpMethod, string uri, HttpContent content = null)
            {
                var httpClient = new HttpClient();
                httpClient.DefaultRequestHeaders.Add("Accept", "application/json, text/javascript, */*; q=0.01");
                httpClient.DefaultRequestHeaders.Add("Cookie", Cookie);

                try
                {
                    if (httpMethod == HttpMethod.Post)
                        return await httpClient.PostAsync(uri, content);
                    else
                        return await httpClient.GetAsync(uri);
                }
                catch (HttpRequestException ex)
                {
                    await _semaphoreSlim.WaitAsync();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.AppendLine($" ERROR! {ex.Message}");
                    await Log(stringBuilder);
                    _semaphoreSlim.Release();

                    return null;
                }
            }

            public async Task UploadLargeFileAsync()
            {
                await PrepareUploadAsync();
                await StartUploadAsync();
                await GetStatusUntilIsDoneAsync();
            }

            private async Task CreateChunks(UploadInfo uploadInfo)
            {
                const int BUFFER_SIZE = 20 * 1024;
                byte[] buffer = new byte[BUFFER_SIZE];

                if (!Directory.Exists(_chunksFolderPath))
                {
                    Directory.CreateDirectory(_chunksFolderPath);
                }
                else
                {
                    Directory.Delete(_chunksFolderPath, true);
                    Directory.CreateDirectory(_chunksFolderPath);
                }

                using (Stream input = File.OpenRead(_filePath))
                {
                    int index = 0;
                    while (input.Position < input.Length)
                    {
                        using (Stream output = File.Create(Path.Combine(_chunksFolderPath, "chunk" + index)))
                        {
                            int remaining = (int)uploadInfo.ChunkSize, bytesRead;
                            while (remaining > 0 && (bytesRead = input.Read(buffer, 0,
                                    Math.Min(remaining, BUFFER_SIZE))) > 0)
                            {
                                output.Write(buffer, 0, bytesRead);
                                remaining -= bytesRead;
                            }
                        }
                        index++;
                    }
                }

                await _semaphoreSlim.WaitAsync();
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.AppendLine($" {uploadInfo.NumChunks} chunks have been created");
                await Log(stringBuilder);
                _semaphoreSlim.Release();
            }

            private static string GetIndex(string filePath)
            {
                var pathParts = filePath.Split('\\');
                return pathParts[^1].Substring(5);
            }

            public async Task GetStatusUntilIsDoneAsync()
            {
                if (_uploadInfo != null)
                {
                    UploadStatus uploadStatus = new UploadStatus() { Status = "pending" };
                    var previousStatus = "pending";
                    var uriBuilder = new UriBuilder(BaseUrl);
                    var uploadTaskId = _uploadInfo.Id;
                    uriBuilder.Path = Path.Combine(uriBuilder.Path, Path.Combine(new string[] { uploadTaskId, "status" }));

                    while (!uploadStatus.Status.Equals("done") && !uploadStatus.Status.Equals("failed"))
                    {
                        var response = await Request(HttpMethod.Get, uriBuilder.Uri.ToString());

                        if (response.StatusCode == System.Net.HttpStatusCode.OK)
                        {
                            var responseContent = response.Content.ReadAsStringAsync();
                            uploadStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<UploadStatus>(responseContent.Result);

                            if (uploadStatus.Status.Equals("failed"))
                            {
                                await _semaphoreSlim.WaitAsync();
                                StringBuilder stringBuilder = new StringBuilder();
                                await LogStatus(uploadStatus.Status);
                                if (uploadStatus.Error != null)
                                {
                                    stringBuilder.AppendLine($" Error value: {uploadStatus.Error.Value}");
                                    stringBuilder.AppendLine($" Error message: {uploadStatus.Error.Message}");

                                }
                                await Log(stringBuilder);
                                _semaphoreSlim.Release();
                            }
                            else
                            {
                                if (uploadStatus.Status.Equals("done"))
                                {
                                    await _semaphoreSlim.WaitAsync();
                                    StringBuilder stringBuilder = new StringBuilder();
                                    await LogStatus(uploadStatus.Status);
                                    if (uploadStatus.Result != null)
                                    {
                                        stringBuilder.AppendLine($" AssetUrl: {uploadStatus.Result.AssetUrl}");
                                    }
                                    await Log(stringBuilder);
                                    _semaphoreSlim.Release();
                                }
                                else
                                {
                                    await _semaphoreSlim.WaitAsync();
                                    StringBuilder stringBuilder = new StringBuilder();
                                    stringBuilder.AppendLine($" Background-task status: {uploadStatus.Status}");
                                    await Log(stringBuilder);
                                    _semaphoreSlim.Release();
                                }
                            }
                            if (previousStatus == uploadStatus.Status)
                            {
                                await Task.Delay(2000);
                            }
                            previousStatus = uploadStatus.Status;
                        }
                        else
                        {
                            await _semaphoreSlim.WaitAsync();
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.AppendLine($" Response: {(Int32)response.StatusCode} - {response.StatusCode}");
                            await Log(stringBuilder);
                            _semaphoreSlim.Release();
                        }
                    }
                }
            }

            public async Task UploadChunksAsync(UploadInfo uploadInfo)
            {
                int uploadedChunks = 0;
                var uploadTaskId = uploadInfo.Id;

                _stopwatch.Start();
                string[] fileEntries = Directory.GetFiles(_chunksFolderPath);
                foreach (string filePath in fileEntries)
                {
                    var uriBuilder = new UriBuilder(BaseUrl);
                    var chunkIndex = GetIndex(filePath);

                    MultipartFormDataContent form = new MultipartFormDataContent();
                    var filebytes = File.ReadAllBytes(filePath);
                    var chunk = new ByteArrayContent(filebytes, 0, filebytes.Length);
                    chunk.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                    chunk.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                    {
                        Name = _fileName,
                        FileName = _fullFileName
                    };
                    form.Add(chunk, "chunk");

                    uriBuilder.Path = Path.Combine(uriBuilder.Path, Path.Combine(new string[] { uploadTaskId, "chunks", chunkIndex }));

                    var response = await Request(HttpMethod.Post, uriBuilder.Uri.ToString(), form);

                    if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
                    {
                        uploadedChunks++;

                        await _semaphoreSlim.WaitAsync();
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.Append($" Chunk uploaded: {uploadedChunks}/{uploadInfo.NumChunks}");
                        stringBuilder.AppendLine(await GetStatusUploadTask(uploadInfo.Id));
                        await Log(stringBuilder);
                        _semaphoreSlim.Release();


                    }
                    else
                    {
                        await _semaphoreSlim.WaitAsync();
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.AppendLine($" Response: {(Int32)response.StatusCode} - {response.StatusCode}");
                        await Log(stringBuilder);
                        _semaphoreSlim.Release();

                        break;
                    }
                }

                if (uploadInfo.HasXmp)
                {
                    await UploadXMPFile(uploadInfo.Id);
                }
                _stopwatch.Stop();

                TimeSpan stopwatchElapsed = _stopwatch.Elapsed;
                await _semaphoreSlim.WaitAsync();
                StringBuilder strBuilder = new StringBuilder();
                string elapsedTime = String.Format("{0:00}:{1:00}:{2:00}.{3:00}", stopwatchElapsed.Hours, stopwatchElapsed.Minutes, stopwatchElapsed.Seconds, stopwatchElapsed.Milliseconds / 10);
                strBuilder.AppendLine($" Upload finished in {elapsedTime}");
                await Log(strBuilder);
                strBuilder.Clear();
                strBuilder.AppendLine($" The background-task is processing the files");
                await Log(strBuilder);
                _semaphoreSlim.Release();
            }

            public async Task UploadXMPFile(string uploadId)
            {
                var uriBuilder = new UriBuilder(BaseUrl);
                var uploadTaskId = uploadId;
                uriBuilder.Path = Path.Combine(uriBuilder.Path, Path.Combine(new string[] { uploadTaskId, "xmp" }));

                MultipartFormDataContent form = new MultipartFormDataContent();
                var filebytes = File.ReadAllBytes(Path.Combine(LargeFilesFolderPath, XMPFileName));
                var chunk = new ByteArrayContent(filebytes, 0, filebytes.Length);
                chunk.Headers.ContentType = new MediaTypeHeaderValue("application/octet-stream");
                chunk.Headers.ContentDisposition = new ContentDispositionHeaderValue("form-data")
                {
                    Name = XMPFileName.Split('.')[0],
                    FileName = XMPFileName
                };
                form.Add(chunk, "chunk");

                var response = await Request(HttpMethod.Post, uriBuilder.Uri.ToString(), form);

                if (response.StatusCode == System.Net.HttpStatusCode.NoContent)
                {
                    await _semaphoreSlim.WaitAsync();
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.Append($" XMP File uploaded");
                    stringBuilder.AppendLine(await GetStatusUploadTask(uploadId));
                    await Log(stringBuilder);
                    _semaphoreSlim.Release();
                }
                else
                {
                    Console.WriteLine($"\t| Response: {(Int32)response.StatusCode} - {response.StatusCode}\n");
                }
            }

            public async Task StartUploadAsync()
            {
                if (_uploadInfo != null)
                    await UploadChunksAsync(_uploadInfo);
            }

            public async Task PrepareUploadAsync()
            {
                _uploadInfo = await CreateUploadTask();
                if (_uploadInfo != null)
                    await CreateChunks(_uploadInfo);
            }

            public async Task<string> GetStatusUploadTask(string uploadId)
            {
                var uriBuilder = new UriBuilder(BaseUrl);
                var uploadTaskId = uploadId;
                var status = "";
                uriBuilder.Path = Path.Combine(uriBuilder.Path, Path.Combine(new string[] { uploadTaskId, "status" }));

                var response = await Request(HttpMethod.Get, uriBuilder.Uri.ToString());

                if (response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    var responseContent = response.Content.ReadAsStringAsync();
                    var uploadStatus = Newtonsoft.Json.JsonConvert.DeserializeObject<UploadStatus>(responseContent.Result);

                    status = $" | Background-task status: {uploadStatus.Status}";
                }
                else
                {
                    status = $" | Response: {(Int32)response.StatusCode} - {response.StatusCode}";
                }

                return status;
            }
        }


        static async Task Main(string[] args)
        {
            var tasks = new List<Task>();
            IList<UploadFile> uploads = new List<UploadFile>();

            // Get the files in the folder and create uploads
            DirectoryInfo dir = new DirectoryInfo(LargeFilesFolderPath);
            for (uint i = 0; i < dir.GetFiles().Length; i++)
            {
                if (!dir.GetFiles()[i].FullName.Contains(".xmp"))
                {
                    uploads.Add(new UploadFile(i, dir.GetFiles()[i].FullName));
                }
            }
            // Create Upload tasks and split file in chunks
            foreach (var upload in uploads)
                tasks.Add(Task.Run(() => upload.UploadLargeFileAsync()));

            await Task.WhenAll(tasks);
            tasks.Clear();
        }

        #region MODELS

        public class FailedResponse
        {
            public string Value { get; set; }
            public string Heading { get; set; }
            public string Description { get; set; }
            public string TechnicalInfo { get; set; }
        }

        public class UploadInfo
        {
            public string Id { get; set; }

            public ulong ChunkSize { get; set; }

            public uint NumChunks { get; set; }

            public bool HasXmp { get; set; }
        }

        public class UploadStatus
        {
            public string Status { get; set; }

            public Result Result { get; set; }

            public Error Error { get; set; }
        }

        public class Result
        {
            public string AssetUrl { get; set; }

            public string AssetDetails { get; set; }
        }

        public class Error
        {
            public string Value { get; set; }

            public string Message { get; set; }
        }

        #endregion
    }
}
